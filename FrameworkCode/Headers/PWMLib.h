#ifndef PWMLib
#define PWMLib

#include "ES_Types.h"

void PWM_Init(void);
void PWM_SetDuty(uint32_t DesiredHighTime);
#endif
