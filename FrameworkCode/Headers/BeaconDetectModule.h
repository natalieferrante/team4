/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef BeaconDetectModule_H
#define BeaconDetectModule_H

#include "ES_Types.h"

// Public Function Prototypes

void InitBeaconDetectModule(void);
bool CheckCenterAlignment(void);
void InitInputCapturePeriod(void);
void InputCaptureResponse(void);

#endif /* BeaconDetectModule_H */
