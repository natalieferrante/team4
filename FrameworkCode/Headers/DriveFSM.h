/****************************************************************************

  Header file for DriveFSM (State Machine)
  Based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DriveFSM_H
#define DriveFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { Waiting, Turning, Aligning, Detecting, Adjusting, Done } DriveState_t;

// Public Function Prototypes
bool InitDriveFSM(uint8_t Priority);
bool PostDriveFSM(ES_Event_t ThisEvent);
ES_Event_t RunDriveFSM(ES_Event_t ThisEvent);

#endif /* Gameplay_Module_H */
