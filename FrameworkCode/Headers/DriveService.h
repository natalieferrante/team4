//Author: Amanda Spyropoulos. Last Update 2/5/2020
#ifndef DriveService_H
#define DriveSService_H

#include "ES_Types.h"
#include "ES_Events.h"

bool InitDriveService(uint8_t Priority);
ES_Event_t RunDriveService(ES_Event_t ThisEvent);
bool PostDriveService(ES_Event_t ThisEvent);

#endif /* Lab8_H */
