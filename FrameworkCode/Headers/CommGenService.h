/****************************************************************************

  Header file for Command Generator service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServComGen_H
#define ServComGen_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"
#include "ES_Events.h"

bool InitCommGenService(uint8_t Priority);
bool PostCommGenService(ES_Event_t ThisEvent);
ES_Event_t RunCommGenService(ES_Event_t ThisEvent);

#endif /* ServComGen_H */
