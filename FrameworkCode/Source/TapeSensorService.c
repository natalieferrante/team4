/****************************************************************************
 Module
   TapeSensorService.c

 Revision
   1.0.1

 Description
   This service contains an event checker that detects that the tape has been encountered.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/05/20 20:32 LG      Created file
 02/07/20       JYC     Updated. Working.
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TapeSensorService.h"
#include "ADMulti.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

#include "DriveService.h"
#include "DriveFSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define TapeSensorTimer 1
#define TAPE_THRESHOLD 150
#define AD_WAIT_TIME 100
#define Num_AD_Pins 1
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
uint32_t        results[1];
static uint8_t  MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitADService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitTapeSensorService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  ThisEvent.EventType = ES_INIT;   // post the initial transition event
  // Initialize Tape sensor timer
  ES_Timer_InitTimer(TapeSensorTimer, AD_WAIT_TIME);
  // Initialize port E
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;
  // Wait for clock
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4)
  {}
  ;
  // Initialize analog pin (PE0)
  ADC_MultiInit(Num_AD_Pins);
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTapeSensorModule

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
****************************************************************************/
bool PostTapeSensorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 * RunTapeSensorService
 * Eventchecker that looks for tape on the ground.
 * Posts to DriveFSM when tape is found.
****************************************************************************/
ES_Event_t RunTapeSensorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  static uint32_t LastTapeSensor;     // Previous value
  uint32_t        CurrentTapeSensor;  // Current value

  // If tape timer times out, check if the tape has been found
  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == TapeSensorTimer))
  {
    ADC_MultiRead(results);             // Read tape analog value
    CurrentTapeSensor = results[0];     // Store value in CurrentTapeSensor

    // If the previous and current values are different AND
    // the current value is within the threshold, the tape has been found
    if ((CurrentTapeSensor != LastTapeSensor) && (CurrentTapeSensor <= TAPE_THRESHOLD))
    {
      ThisEvent.EventType = TAPE_FOUND;
      PostDriveFSM(ThisEvent);  // Post that the tape was found to DriveFSM
    }
    LastTapeSensor = CurrentTapeSensor;                 // Store current value as previous value
    ES_Timer_InitTimer(TapeSensorTimer, AD_WAIT_TIME);  // Reinitialize timer
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
