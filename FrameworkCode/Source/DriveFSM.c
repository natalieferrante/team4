/*DriveFSM Code - State Machine - Uses the Gen2.x Event Framework
 * Author: Julea Chin
 * Date: 2/5/2020
****************************************************************************/

/*----------Header Files----------*/

// header files for this service
#include "DriveFSM.h"
#include "InterruptLib.h"

// standard header files
#include <stdio.h>
#include <stdlib.h>

// include header files for hardware access
#include <stdint.h>
#include "inc/hw_timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "bitdefs.h"
#include "termio.h"

// include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// include header files for the other modules in Lab8 that are referenced
#include "DriveService.h"
#include "BeaconDetectModule.h"

/*----------#defines----------*/

//Event Parameters
#define LEFT 1
#define RIGHT 2
#define HALF 1
#define FULL 2
#define SLOW 1

/*----------Module Variables----------*/

static uint8_t      MyPriority;
static DriveState_t CurrentState;

/*----------Function Prototypes----------*/

/*----------Module Code----------*/

/****************************************************************************
 * InitDriveFSM
 * Takes a priority number, returns true
****************************************************************************/
bool InitDriveFSM(uint8_t Priority) // takes a priority number, returns true
{
  //Initialize MyPriority variable with passed in priority number
  MyPriority = Priority;

  //Set CurrentState to be Waiting for commands
  CurrentState = Waiting;

  //Initialize modules
  InitBeaconDetectModule();

  //Post Event ES_Init to DriveFSM queue (this service)
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//End of DriveFSM

/****************************************************************************
 * PostDriveFSM
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostDriveFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

//End of DriveFSM

/****************************************************************************
 * RunDriveFSM - Drive Mode State Machine
 * Command Generated Events: FORWARD, BACKWARD, TURN, STOP, ALIGN, DETECT
 * Sensed Events: BECAME_ALIGNED, BECAME_UNALIGNED, TAPE_FOUND
****************************************************************************/
ES_Event_t RunDriveFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; //assuming no errors

  //Declaring local variable NextState and setting equal to CurrentState
  static DriveState_t NextState;
  NextState = CurrentState;

  //Switch statement based on state machine for driving
  switch (CurrentState)
  {
    // Waiting State:
    // Handles stop, forward motion, and backwards motion
    case Waiting:
    {
      // puts("\n\rCurrent State: Waiting");

      switch (ThisEvent.EventType)
      {
        case STOP:
        {
          puts("\n\rHandling STOP command.");
          //Stays in Waiting state
          //puts("\n\rHandling STOP command.");
          //Post, to stop the robot from moving
          PostDriveService(ThisEvent);
          break;
        }

        case FORWARD:
        {
          //Stays in Waiting state
          puts("\n\rHandling FORWARD command.");
          //Post, to move robot forward
          PostDriveService(ThisEvent);
          break;
        }

        case BACKWARD:
        {
          //Stays in Waiting state
          puts("\n\rHandling BACKWARD command.");
          //Post, to move robot backwards
          PostDriveService(ThisEvent);
          break;
        }

        case TURN:
        {
          //Go to Turning state
          NextState = Turning;
          puts("\n\rHandling TURN command.");

          //Post, to start turning the robot
          PostDriveService(ThisEvent);
          break;
        }

        case ALIGN:
        {
          //Go to Aligning state if currently unaligned with beacon
          NextState = Aligning;
          puts("\n\rHandling ALIGN command.");

          // Post, to start turning the robot (slowly) until aligned
          ES_Event_t PostEvent;
          PostEvent.EventType = TURN;
          PostEvent.EventParam = RIGHT;
          PostDriveService(PostEvent);
          break;
        }

        case DETECT:
        {
          //Go to Detecting state
          NextState = Detecting;
          puts("\n\rHandling DETECT command.");

          // Post, to start moving forward until tape is detected
          ES_Event_t PostEvent;
          PostEvent.EventType = FORWARD;
          PostEvent.EventParam = HALF;
          PostDriveService(PostEvent);
          break;
        }

        default:  // Error handler
        {}
        break;
      }

      break; // End of Waiting
    }

    case Turning:
    {
      puts("\n\rCurrent State: Turning");

      // Proper turn achieved when Turn Timer times out
      if (ThisEvent.EventType == ES_TIMEOUT)
      {
        //Post, to stop the robot from turning
        ES_Event_t PostEvent;
        PostEvent.EventType = STOP;
        PostDriveService(PostEvent);

        //Go to Waiting state
        puts("\n\rTurn complete. Back to Waiting.");
        NextState = Waiting;
      }

      break;  // End of Turning
    }

    case Aligning:
    {
      puts("\n\rCurrent State: Aligning");
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT0HI); // testing set the LED on

      // Check if the robot is aligned with beacon
      if (ThisEvent.EventType == BECAME_ALIGNED)
      {
        printf("\n\r  Became aligned!");
        //Post, to stop the robot from turning
        ES_Event_t PostEvent;
        PostEvent.EventType = STOP; //assuming no errors
        PostDriveService(PostEvent);

        //Go to Waiting state
        puts("\n\rAlign complete. Back to Waiting.");
        NextState = Waiting;
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO); // testing set the LED off
      }

      // Escape Route (for key press testing -- can force a STOP)
      if (ThisEvent.EventType == STOP)
      {
        puts("\n\rEscape.");
        NextState = Waiting;
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO); // testing set the LED off
      }

      break;  // End of Aligning
    }

    case Detecting:
    {
      puts("\n\rCurrent State: Detecting");
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT1HI); // testing set the LED on

      if (ThisEvent.EventType == TAPE_FOUND)
      {
        //Post, to stop the robot from moving
        ES_Event_t PostEvent;
        PostEvent.EventType = STOP; //assuming no errors
        PostDriveService(PostEvent);

        //Go to Waiting state
        puts("\n\rTape found. VICTORY!");
        NextState = Done;
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT1LO); // testing set the LED off
      }

      break; // End of Detecting
    }

    default:  // Error handler
    {}
    break;
  }

  // Set current state to next state
  CurrentState = NextState;

  // If no errors, should return NO EVENT
  return ReturnEvent;
}

// End of DriveFSM
