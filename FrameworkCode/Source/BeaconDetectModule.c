/****************************************************************************
 Module
   BeaconDetectService.c

 Revision
   1.0.1

 Description
   This module is responsible for reporting changes in alignment and posting events to let the drive state machine understand the current status of alignment.

 Notes
 I need to write:
 - A getter function that returns a bool (true or false) based on alignment currently of the center phototransistor
 - An event checker that detects if the center phototransistor has become unaligned and sends a parameter (if applicable) to help it steer appropriately
 - Functions to read all of the phototransistors for alignment - maybe it makes sense to put this in an array?

 History
 When             Who   What/Why
 --------------   ---   --------
 02/04/2020 20:46 LG    Started writing module
 02/05/2020 13:10 LG    Updated module with additional functions
 02/05/2020 13:10 JC    Updated module to handle only 1 IR sensor
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"

// the headers to access the GPIO subsystem
#include "inc/hw_sysctl.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_Timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"

#include "BeaconDetectModule.h"
#include "DriveFSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define IRPIN_HI BIT2HI
#define IRPIN_LO BIT2LO
#define lowThresh 26000
#define highThresh 28000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint32_t Period;
static uint32_t LastCapture;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitBeaconDetectService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Lisa Gardner, 02/04/20, 21:05
****************************************************************************/
void InitBeaconDetectModule()
{
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5;
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5)
  {}
  ;
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= IRPIN_HI;
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) &= IRPIN_LO;

  InitInputCapturePeriod();
}

/****************************************************************************
 Function
  CheckCenterAlignment

 Parameters
  ES_Event_t : the event to process

 Returns
  BECAME_ALIGNED or BECAME_UNALIGNED
  ES_NO_EVENT if no error ES_ERROR otherwise

 Description
  Event checker to see if the center is aligned

 Author
Lisa Gardner, 02/04/20, 21:12
****************************************************************************/

bool CheckCenterAlignment()
{
  static uint32_t LastPeriod;

  ES_Event_t      ThisEvent;
  bool            ReturnVal = false;

  //Set Alignment to state read from port pin
  //If LO, then unaligned (FALSE). If HI, then aligned (TRUE).

  //If the Alignment is different from the LastAlignment
  if ((Period > lowThresh) && (Period < highThresh) && (LastPeriod > lowThresh) && (LastPeriod < highThresh))
  {
    ReturnVal = true; // Set ReturnVal = True

    // If the center phototransistor is currently aligne
    ThisEvent.EventType = BECAME_ALIGNED;
    PostDriveFSM(ThisEvent);
  }

  LastPeriod = Period;
  return ReturnVal; // Return ReturnVal
}

/****************************************************************************
 * InitInputCapturePeriod
 * Uses Timer A in Wide Timer 0 to capture the input
 * Julea Chin
****************************************************************************/
void InitInputCapturePeriod(void)
{
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER0_BASE + TIMER_O_TAPR) = 0;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xfff0ffff) + (7 << 16);
  // make pin 4 on Port C into an input (to read from the IR LED)
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT4HI);
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 so appears in EN2 at bit 30
  HWREG(NVIC_EN2) |= BIT30HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 * InputCaptureResponse
 * Calculates the period between high edges sensed by the IR Receiver
 * Julea Chin
****************************************************************************/
void InputCaptureResponse(void)
{
  uint32_t ThisCapture;
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT; // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);
  Period = ThisCapture - LastCapture;
  // update LastCapture to prepare for the next edge
  LastCapture = ThisCapture;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
