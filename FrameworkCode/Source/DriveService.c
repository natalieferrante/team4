/****************************************************************************
 Module
   DriveService.c

 Revision
   1.0.1

 Description
   This service responds to DriveFSM requests to control the motors.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/05/20 20:32 AS      Created file
 02/07/20       JYC     Updated. Working.
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include "DriveService.h"
#include "ES_Events.h"     /* gets ES_Events */

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include <stdint.h>
#include "inc/hw_pwm.h"
#include "bitdefs.h"
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab8 that are referenced */
#include "CommGenService.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*----------------------------- Module Defines ----------------------------*/
#define TicksPerMS 40000
// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerMS 40000 / 32
// set 1kHz frequency so 1mS period. UPDATE @Amanda
#define PeriodInMS 1
// program generator A to go to 1 at rising comare A, 0 on falling compare A
#define GenA_Normal_0 (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO)
#define GenA_Normal_1 (PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO)
// program generator B to go to 1 at rising comare B, 0 on falling compare B
#define GenB_Normal_0 (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO)
#define GenB_Normal_1 (PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO)
#define BitsPerNibble 4
#define msInSec 1000        // constant - 1000 ms per second
#define secInMin 60         // constant - 60 sec per min
#define MAX_DUTY_CYCLE 100  // 100% is max DC
#define DUTY_CYCLE_STEP 5   // DC increment for calibration
#define MIN_DUTY 0          // min duty cycle to turn motor UPDATE @AMANDA
#define HALF_DUTY 50
#define FULL_DUTY 98
// pins associated with each motor
#define LEFT_MOTOR_A 6
#define LEFT_MOTOR_B 7
#define RIGHT_MOTOR_A 4
#define RIGHT_MOTOR_B 5
// convention: When the motor is going forward, A should be high and B should be low on both the L and R motors
#define L 1
#define R 2
#define HALF 1
#define FULL 2

/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority; // // priority of service
// module functions
static void initPins(void);
static void stop(void);
static void forward(uint16_t duty_cycle);
static void reverse(uint16_t duty_cycle);
static void turn_right(uint16_t duty_cycle);
static void turn_left(uint16_t duty_cycle);
static void RestoreDC(void);

/****************************************************************************
 * InitDriveService
 * Takes a priority number, returns true
****************************************************************************/
bool InitDriveService(uint8_t Priority)
{
  MyPriority = Priority;
  initPins(); // initialize the PWM pins
  stop();     // begin in a stopped state

  ES_Event_t ThisEvent;
  PostCommGenService(ThisEvent);

  return true;
}

/****************************************************************************
 * RunDriveService
 * Receives an event from DriveFSM
 * Moves the robot forward/backward/turn at full or half speed, as needed
****************************************************************************/
ES_Event_t RunDriveService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (ThisEvent.EventType)
  {
    case FORWARD:
    {
      if (ThisEvent.EventParam == HALF)
      {
        forward(HALF_DUTY);
      }
      else
      {
        forward(FULL_DUTY);
      }
    } break;
    case BACKWARD:
    {
      if (ThisEvent.EventParam == HALF)
      {
        reverse(HALF_DUTY);
      }
      else
      {
        reverse(FULL_DUTY);
      }
    }
    break;
    case TURN:
    {
      if (ThisEvent.EventParam == L)
      {
        turn_left(HALF_DUTY);
      }
      else
      {
        turn_right(HALF_DUTY);
      }
    } break;
    case STOP:
    {
      stop();
    }
    break;
    default:
    {
      ;
    }
    break;
  }

  return ReturnEvent;
}

/****************************************************************************
 * PostDriveService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostDriveService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 * RestoreDC
 * Call this function to revert to DC between 1-99
****************************************************************************/
static void RestoreDC(void)
{
  // To restore the previous DC, simply set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal_0;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENA) = GenA_Normal_1;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = GenB_Normal_1;
}

// reverse
// The robot will turn right at the given duty cycle
static void reverse(uint16_t duty_cycle)
{
  RestoreDC();  // restores any pins

  if ((duty_cycle > 0) && (duty_cycle < 100))
  {
    HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_1_CMPB) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENA_ACTZERO_ZERO;                                      // set pin to LOW
    HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENB_ACTZERO_ZERO;                                      // set pin to LOW
  }
}

// forward
// The robot will turn right at the given duty cycle
static void forward(uint16_t duty_cycle)
{
  RestoreDC();

  if ((duty_cycle > 0) && (duty_cycle < 100))
  {
    HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_1_CMPA) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;                                      // set pin to LOW
    HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;                                      // set pin to LOW
  }
}

// turn_right
// The robot will reverse at the given duty cycle
static void turn_right(uint16_t duty_cycle)
{
  RestoreDC();

  if ((duty_cycle > 0) && (duty_cycle < 100))
  {
    HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_1_CMPB) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;                                      // set pin to LOW
    HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENA_ACTZERO_ZERO;                                      // set pin to LOW
  }
}

// turn_left
// The robot will drive forward at the given duty cycle
static void turn_left(uint16_t duty_cycle)
{
  RestoreDC();

  if ((duty_cycle > 0) && (duty_cycle < 100))
  {
    HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_1_CMPA) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - duty_cycle)); // set DC
    HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;                                      // set pin to LOW
    HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;                                      // set pin to LOW
  }
}

// stop
// Sets the duty cycle on all four pins to 0. Stops the robot.
static void stop(void)
{
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO; // set all pins to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENA_ACTZERO_ZERO;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;
}

/* initPins
 * Initialize all four PWM pins for the robot.
 * PB 4 = Left Motor
 * PB 5 = Left Motor
 * PB 6 = Right Motor
 * PB 7 = Right Motor
*/
static void initPins(void)
{
// start by enabling the clock to the PWM Module (PWM0) for PB 4-7
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
// enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
// Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

// make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    ;
  }
// disable the PWM while initializing for all four pins
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;
  HWREG(PWM0_BASE + PWM_O_1_CTL) = 0;

// program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
// for all four pins
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENA) = GenA_Normal_1;

  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = GenB_Normal_1;

// Set the PWM period. Since we are counting both up & down, we initialize
// the load register to 1/2 the desired total period. We will also program
// the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInMS * PWMTicksPerMS)) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_LOAD) = ((PeriodInMS * PWMTicksPerMS)) >> 1;

// Set the initial Duty cycle on all four pins to 50% by programming the compare value
// to 1/2 the period to count up (or down). Technically, the value to program
// should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
// the period, we can skip the subtract
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = HWREG(PWM0_BASE + PWM_O_1_LOAD) >> 1;

// enable the PWM outputs for all four pins
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN);

// now configure the Port B pins to be PWM outputs
// start by selecting the alternate function for PB4-7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= BIT4HI | BIT5HI | BIT6HI | BIT7HI;

// now choose to map PWM to those pins, this is a mux value of 4 that we
// want to use for specifying the function on bits 6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x00ffffff)
      + (4 << (4 * BitsPerNibble)) + (4 << (5 * BitsPerNibble)) + (4 << (6 * BitsPerNibble)) + (4 << (7 * BitsPerNibble));

  // Enable pins 6 & 7 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT7HI | BIT6HI | BIT5HI | BIT4HI;

  // Enable pins 0 & 1 on Port B for testing if we are in aligning (BIT0) and tape detecting (BIT1)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI);

  // make pins 6 & 7 on Port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);

  // Make pins 0 & 1 on Port B outputs for testing if we are in aligning (BIT0) and tape detecting (BIT1)
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI);

  // Set the testing pins low, so the LED is off initially
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO);

// set the up/down count mode, enable the PWM generator and make
// both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
      PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  HWREG(PWM0_BASE + PWM_O_1_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
      PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}
