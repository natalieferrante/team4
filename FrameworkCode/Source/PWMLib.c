/****************************************************************************
 Module
   PWMLib.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to the Tiva PWM library.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 10/17/19 644   nat     lab3 usage
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_Timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

#include "ES_Configure.h"
#include "ES_Framework.h"

//From Ed's PWM lib
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"

#include "PWMLib.h"
#include "BITDEFS.H"

/*----------------------------- Module Defines ----------------------------*/

#define GET_MSB_IN_LSB(x) ((x & 0x80) >> 7)

/*---------------------------- Module Variables ---------------------------*/
uint32_t        GenA_Normal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO);
uint32_t        GenB_Normal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO);
uint32_t        BitsPerNibble = 4;
//this frequency is 1/(time constant) determined in part 1
static uint32_t PeriodInMS = 195;     // in micro seconds
static uint32_t PWMTicksPerMS = 1250; // based on 40MHz sysclock, 32 sys ticks/s
// 40MHz/32 = 1250

/*---------------------------- Module Functions ---------------------------*/

void RestoreDC(void);

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     PWM_Init

 Returns
     void

 Description
    Initialize the MyPriority variable with the passed in parameter.
    Initialize all PWM hardware
 Notes

 Author
     Natalie Ferrante, 1/16/20, 3:15
****************************************************************************/
void PWM_Init(void)
{
  // start by enabling the clock to the PWM Module (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;

  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

  // Select the PWM clock as System Clock/32 bit
  //enough resolution based on 10,000Hz freq required in pt. 3
  // (but can increase in want to to 64!)
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  // make sure that the PWM module clock has gotten going ("wait")
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    ;
  }

  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;

  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInMS * PWMTicksPerMS / 1000)) >> 1;

  // Set the initial Duty cycle on A to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;

  // Set the initial Duty cycle on B to 0%: simply set the action on Zero
  //to set the output to zero
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;

  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM0EN);

  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT7HI | BIT6HI);

  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x00ffffff) + (4 << (7 * BitsPerNibble)) +
      (4 << (6 * BitsPerNibble));

  // Enable pins 6 & 7 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT7HI | BIT6HI);

  // make pins 6 & 7 on Port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT7HI | BIT6HI);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
      PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}

/****************************************************************************
 Function
     PWM_SetDuty

 Returns
     void

 Description
    Sets Duty Cycle on motor based on input from external potentiometer pin
 Notes

 Author
     Natalie Ferrante, 1/16/20, 4:08
****************************************************************************/
void PWM_SetDuty(uint32_t DesiredDutyCycle)
{
  uint32_t input;
  //set duty if at 0
  if (DesiredDutyCycle == 0)
  {
    input = PWM_0_GENA_ACTZERO_ZERO;

    //set duty if at 100
  }
  else if (DesiredDutyCycle == 100)
  {
    input = PWM_0_GENA_ACTZERO_ONE;

    //set duty if at anything else
  }
  else
  {
    RestoreDC();
    HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) * (100 - DesiredDutyCycle)) / 100;
  }
}

/****************************************************************************
 Function
     PWM_SetPeriod

 Returns
     void

 Description
    restore the DC control for PWM_SetDuty after 100 or 0 DC
 Notes

 Author
     Natalie Ferrante, 1/30/20
****************************************************************************/
void RestoreDC(void)
{
  // To restore the previos DC, simply set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;
}
